using UnrealBuildTool;

public class My2DTarget : TargetRules
{
	public My2DTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("My2D");
	}
}
